const userRouter = require("express").Router();

const userController = require("../controllers/userCtrl");
const checkToken = require("../helpers/middlewares/checkToken");
const upload = require("../helpers/middlewares/upload");
const uploadImage = require("../helpers/middlewares/imgUpload");


userRouter.get("/:id", checkToken.login, userController.getUser);
userRouter.get("/info/:id" , uploadImage.multiUpload , userController.updateProfile)
userRouter.patch("/photo/:id" , upload, userController.changePhoto)

module.exports = userRouter;
